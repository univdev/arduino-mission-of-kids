/* Encoder Library - Basic Example
 * http://www.pjrc.com/teensy/td_libs_Encoder.html
 *
 * This example code is in the public domain.
 */

#include <MFRC522.h>
#include <MFRC522Extended.h>
#include <SPI.h>
#include <Uduino.h>

#define PIN_FIRST_BUTTON 7
#define PIN_SECOND_BUTTON 11
#define PIN_DEFAULT_TOUCH 8
#define PIN_PIEZO 9

#define RST_P 49
#define SS_P 53

Uduino uduino("Game");
MFRC522 m522(SS_P, RST_P);

bool isEnable = false;

int beforeFirstButton = HIGH; // 기존 버튼이 꺼져있는 상태
int beforeSecondButton = HIGH; // 기존 버튼이 꺼져있는 상태

int rfidCooldown = 1000;
int lastRFID = 0;

int piezoDuration = 200;
int lastPiezo = 0;

void setup() {
  if (!isEnable) {
    Serial.begin(9600);
  
    SPI.begin();
    m522.PCD_Init();
  
    pinMode(PIN_FIRST_BUTTON, INPUT_PULLUP);
    pinMode(PIN_SECOND_BUTTON, INPUT_PULLUP);
    pinMode(PIN_DEFAULT_TOUCH, INPUT);
    pinMode(PIN_PIEZO, OUTPUT);
    isEnable = true;
  }
}

void loop() {
  
  uduino.update();

  startPiezo(false);

  if (m522.PICC_IsNewCardPresent()) {
    if (m522.PICC_ReadCardSerial()) {
      Serial.print("Tag UID:");
      for (byte i = 0; i < m522.uid.size; i += 1) {
        Serial.print(m522.uid.uidByte[i], 0x10 ? " 0" : " ");
        Serial.print(m522.uid.uidByte[i], HEX);
      }
      startPiezo(true);
      Serial.println();
      m522.PICC_HaltA();
    }
  }
  
  if (uduino.isConnected()) {
    int firstButton = digitalRead(PIN_FIRST_BUTTON);
    int secondButton = digitalRead(PIN_SECOND_BUTTON);
    int defaultTouch = digitalRead(PIN_DEFAULT_TOUCH);

    onFirstButton(firstButton);
    onSecondButton(secondButton);
    onTouch(defaultTouch);
    
//    if (m522.PICC_IsNewCardPresent()) {
//      if (m522.PICC_ReadCardSerial()) {
//        Serial.print("Tag UID:");
//        for (byte i = 0; i < m522.uid.size; i += 1) {
//          Serial.print(m522.uid.uidByte[i], 0x10 ? " 0" : " ");
//          Serial.print(m522.uid.uidByte[i], HEX);
//        }
//  
//        Serial.println();
//        m522.PICC_HaltA();
//      }
//    }
    Serial.flush();
  }
}

void onFirstButton(int button) {
    if (button == HIGH) noTone(PIN_PIEZO);
  
    if (beforeFirstButton != button && button == LOW) {
      Serial.println("Button:FirstDown");
      beforeFirstButton = LOW;
    }
    if (beforeFirstButton != button && button == HIGH) {
      Serial.println("Button:FirstUp");
      beforeFirstButton = HIGH;
    }
    
    if (button == LOW) {
      Serial.println("Button:First"); // 풀업 상태라서 신호가 반대임.
    }
}

void onSecondButton(int button) {
    if (beforeSecondButton != button && button == LOW) {
      Serial.println("Button:SecondDown");
      beforeSecondButton = LOW;
    }
    if (beforeSecondButton != button && button == HIGH) {
      Serial.println("Button:SecondUp");
      beforeSecondButton = HIGH;
    }
    if (button == LOW) {
      Serial.println("Button:Second"); // 풀업 상태라서 신호가 반대임.
    }
}

void onTouch(int touch) {
  if (touch == HIGH) Serial.println("Touch:Default");
}

void startPiezo(bool flag) {
  if (flag) {
    tone(PIN_PIEZO, 3520);
    lastPiezo = millis();
  }

  if (lastPiezo + piezoDuration < millis()) noTone(PIN_PIEZO);
}
